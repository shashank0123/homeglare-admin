import React from "react"
import Router from "./Router"
import "./components/@vuexy/rippleButton/RippleButton"
import setAuthToken from "./utility/settoken"
import {store} from "./redux/storeConfig/store";
import {validate,loadingfalse} from "./redux/actions/auth/loginActions"

import "react-perfect-scrollbar/dist/css/styles.css"
import "prismjs/themes/prism-tomorrow.css"

// Check for token to keep user logged in
if (localStorage.token) {
  const token = localStorage.token;
  setAuthToken(token);
  store.dispatch(validate(token));
}
else{
  store.dispatch(loadingfalse())
}

const App = props => {
  return <Router />
}

export default App
