// Pages
import {
  BackendError,
  Blank,
  Google,
  Home,
  Invoice,
  List,
  AdminList,
  VendorList,
  OnlineList,
  Lockscreen,
  AddProduct,
  ProductList,
  Media,
  NotFound,
  PasswordReset,
  PricingPage,
  Signin,
  Signup,
  TimelinePage,
  Widgets,
  AddCategory,
  CategoryList,
} from './pages';

import AppsIcon from '@material-ui/icons/Apps';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import EqualizerIcon from '@material-ui/icons/Equalizer';
// Icons
import ExploreIcon from '@material-ui/icons/Explore';
import FaceIcon from '@material-ui/icons/Face';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import Looks3Icon from '@material-ui/icons/Looks3';
import Looks4Icon from '@material-ui/icons/Looks4';
import NavigationIcon from '@material-ui/icons/Navigation';
import PagesIcon from '@material-ui/icons/Pages';
import PersonIcon from '@material-ui/icons/Person';
import PhotoIcon from '@material-ui/icons/Photo';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import categoryList from './pages/categories/categoryList';

export default {
  items: [
    {
      path: '/',
      name: 'Home',
      type: 'link',
      icon: ExploreIcon,
      component: Home
    },
    {
      path: '/users',
      name: 'Users',
      type: 'submenu',
      icon: AppsIcon,
      badge: {
        type: 'primary',
        value: '2'
      },
      children: [
        {
          path: '/userlist',
          name: 'User List',
          component: List
        },
        {
          path: '/vendors',
          name: 'Vendor List',
          component: VendorList
        },
        {
          path: '/admins',
          name: 'Admin List',
          component: AdminList
        },
        {
          path: '/online',
          name: 'Online Users',
          component: OnlineList
        },
        {
          path: '/statistics-users',
          name: 'Total Statistics',
          component: Widgets
        }
      ]
    },
    {
      path: '/products',
      name: 'Products',
      type: 'submenu',
      icon: PhotoIcon,
      children: [
        {
          path:'/add-product',
          name:'Add Product',
          component: AddProduct
        },
        {
          path:'/products',
          name:'Product List',
          component: ProductList
        },
        {
          path:'/categorywiseproduct',
          name:'Product By Category',
          component: 'ProductByCategory'
        },
      ]
    },
    {
      path: '/categories',
      name: 'Categories',
      type: 'submenu',
      icon: EqualizerIcon,
      badge: {
        type: 'error',
        value: '10'
      },
      children: [
        {
          path:'/add-category',
          name:'Add category',
          component: AddCategory
        },
        {
          path:'/categories',
          name:'Category List',
          component: CategoryList
        },
      ]
    },
    {
      path: '/media',
      name: 'Media',
      type: 'link',
      icon: Looks3Icon,
      component:Media
    },
    {
      path: '/orders',
      name: 'Orders',
      type: 'submenu',
      icon: Looks3Icon,
      children: [
        {
          path:'total-orders',
          name:'Order List',
          component: 'Orders'
        },
        {
          path:'pending-orders',
          name:'Pending Orders',
          component: 'PendingOrders'
        },
        {
          path:'delivered-orders',
          name:'Delivered Order',
          component: 'DeliveredOrders'
        },
        {
          path:'processing-orders',
          name:'In Process Orders',
          component: 'ProcessingOrders'
        },
      ]
    },
    {
      path: '/reports',
      name: 'Reports',
      type: 'submenu',
      icon: Looks4Icon,
      badge: {
        type: 'secondary',
        value: 'R'
      },
      children: [
        {
          path: '/sales-report',
          name: 'Sales Report',
          component: 'SalesReport'
        },
        {
          path: '/delivery-report',
          name: 'Delivery Report',
          component: 'DeliveryReport'
        },
        {
          path: '/cancellation-report',
          name: 'Cancellation Report',
          component: 'CancellationReport'
        },
      ]
    },
    {
      path: '/inventories',
      name: 'Inventory',
      type: 'link',
      icon: ViewColumnIcon,
      children: [
        {
          path: '/inventories',
          name: 'Product Inventory',
          component: 'ProductInventory'
        },
        {
          path: '/add-inventory',
          name: 'Add Inventory',
          component: 'AddInventory'
        },
      ]
    },
    {
      path: '/invoices',
      name: 'Invoice',
      type: 'submenu',
      icon: ShowChartIcon,
      children: [
        {
          path: '/all-invoices',
          name: 'Invoice List',
          component: 'InvoiceList'
        },
      ]
    },
    {
      path: '/gst',
      name: 'GST',
      type: 'submenu',
      icon: NavigationIcon,
      children: [
        {
          path: '/view-gst-details',
          name: 'Company GST',
          component: Google
        },
        {
          path: '/gst-report',
          name: 'GST Report',
          component: 'GSTReport'
        }
      ]
    },
    {
      path: '/queries',
      name: 'Queries',
      type: 'submenu',
      icon: PagesIcon,
      children: [
        {
          path: '/seller-queries',
          name: 'Seller Queries',
          component: Invoice
        },
        {
          path: '/timeline',
          name: 'Timeline',
          component: TimelinePage
        },
        {
          path: '/blank',
          name: 'Blank',
          component: Blank
        },
        {
          path: '/pricing',
          name: 'Pricing',
          component: PricingPage
        }
      ]
    },
    {
      name: 'Site Settings',
      type: 'submenu',
      icon: PersonIcon,
      children: [
        {
          path: '/signin',
          name: 'Signin',
          component: Signin
        },
        {
          path: '/signup',
          name: 'Signup',
          component: Signup
        },
        {
          path: '/forgot',
          name: 'Forgot',
          component: PasswordReset
        },
        {
          path: '/lockscreen',
          name: 'Lockscreen',
          component: Lockscreen
        }
      ]
    },
    {
      name: 'Offers',
      type: 'submenu',
      icon: FaceIcon,
      children: [
        {
          path: '/404',
          name: '404',
          component: NotFound
        },
        {
          path: '/500',
          name: 'Error',
          component: BackendError
        }
      ]
    },
    {
      path: 'https://iamnyasha.github.io/react-primer-docs/',
      name: 'Insights',
      type: 'external',
      icon: LocalLibraryIcon
    },
    {
      path: 'https://primer.fusepx.com',
      name: 'Coming Soon',
      type: 'external',
      icon: BookmarkIcon
    },
    {
      path: '/signin',
      name: 'Login',
      icon: BookmarkIcon,
      component:Signin

    }
  ]
};
