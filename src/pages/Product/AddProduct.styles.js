const styles = theme => ({
    wrapper: {
      flex: "none",
      maxWidth: "600px",
      width: "100%",
      margin: "0 auto"
    },
    root: {
      width: '100%',
      marginTop: theme.spacing(1) * 3,
    },
    formControl: {
      width: "100%",
      minWidth: '500px',
      margin: '0 auto'
    },
  
    formControlBtn: {
      height:' 54px',
      margin: '5px 5px',
      border: '1px solid #bbb',
      width:' 80px',
      padding: '0 10px',
      background: '#e0e0e0',
      color: '#000',
      borderRadius:' 5px'
    },
  
    imageFormControl: {
      width: '100%',
      opacity: 0,
      height: 0
    },
    productImagesContainer: {
      display: 'flex',
      flexDirection: 'row',
      padding: '18px',
      flexWrap: 'wrap'
    },
    productImage: {
      height: '189px',
      padding: '2%',
      margin: '0 auto',
      width: '96%'
    },
    crossIcon: {
      float: 'right',
      padding: '5px 0 5px 10px',
      border: '1px solid #000',
      borderRadius: '79%',
      height: '20px',
      fontSize: '14px',
      width: '20px',
    },
    formHeading: {
      textAlign: 'center'
    },
    formGrid: {
      margin: '0 auto'
    },
    mxy10: {
      margin: '10px'
    },
    my10: {
      margin: '10px 0'
    },
    mx10: {
      margin: '0 10px'
    }
    
  });

export default styles;
export { styles };