// import React from 'react';
// import {Wrapper } from '../../components/'
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
// import Table from '@material-ui/core/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableFooter from '@material-ui/core/TableFooter';
// import TablePagination from '@material-ui/core/TablePagination';
// import TableRow from '@material-ui/core/TableRow';
// import Paper from '@material-ui/core/Paper';
// import IconButton from '@material-ui/core/IconButton';
// import FirstPageIcon from '@material-ui/icons/FirstPage';
// import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
// import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
// import LastPageIcon from '@material-ui/icons/LastPage';

// const actionsStyles = theme => ({
//   root: {
//     flexShrink: 0,
//     color: theme.palette.text.secondary,
//     marginLeft: theme.spacing(1) * 2.5,
//   },
// });

// class TablePaginationActions extends React.Component {
//   handleFirstPageButtonClick = event => {
//     this.props.onChangePage(event, 0);
//   };

//   handleBackButtonClick = event => {
//     this.props.onChangePage(event, this.props.page - 1);
//   };

//   handleNextButtonClick = event => {
//     this.props.onChangePage(event, this.props.page + 1);
//   };

//   handleLastPageButtonClick = event => {
//     this.props.onChangePage(
//       event,
//       Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
//     );
//   };

//   render() {
//     const { classes, count, page, rowsPerPage, theme } = this.props;

//     return (
//       <div className={classes.root}>
//         <IconButton
//           onClick={this.handleFirstPageButtonClick}
//           disabled={page === 0}
//           aria-label="First Page"
//         >
//           {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
//         </IconButton>
//         <IconButton
//           onClick={this.handleBackButtonClick}
//           disabled={page === 0}
//           aria-label="Previous Page"
//         >
//           {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
//         </IconButton>
//         <IconButton
//           onClick={this.handleNextButtonClick}
//           disabled={page >= Math.ceil(count / rowsPerPage) - 1}
//           aria-label="Next Page"
//         >
//           {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
//         </IconButton>
//         <IconButton
//           onClick={this.handleLastPageButtonClick}
//           disabled={page >= Math.ceil(count / rowsPerPage) - 1}
//           aria-label="Last Page"
//         >
//           {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
//         </IconButton>
//       </div>
//     );
//   }
// }

// TablePaginationActions.propTypes = {
//   classes: PropTypes.object.isRequired,
//   count: PropTypes.number.isRequired,
//   onChangePage: PropTypes.func.isRequired,
//   page: PropTypes.number.isRequired,
//   rowsPerPage: PropTypes.number.isRequired,
//   theme: PropTypes.object.isRequired,
// };

// const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
//   TablePaginationActions,
// );

// let counter = 0;
// function createData(name, calories, fat) {
//   counter += 1;
//   return { id: counter, name, calories, fat };
// }

// const styles = theme => ({
//   root: {
//     width: '100%',
//     marginTop: theme.spacing(1) * 3,
//   },
//   table: {
//     minWidth: 500,
//   },
//   tableWrapper: {
//     overflowX: 'auto',
//   },
// });

// class AddProduct extends React.Component {
//   state = {
//     rows: [
//       createData('Shashank', 'Shashank@gmail.com', 'user',.7),
//       createData('Kuldeep', 'Kuldeep@gmail.com', 'user',.0),
//       createData('Mahesh', 'Mahesh@gmail.com', 'user',.0),
//       createData('Shubham', 'Shubham@gmail.com', 'user',.0),
//       createData('Bibhu', 'Bibhu@gmail.com', 'user',.0),
//       createData('Shivam', 'Shivam@gmail.com', 'user',.2),
//       createData('Ashish', 'Ashish@gmail.com', 'user',.0),
//       createData('Summit', 'Summit@gmail.com', 'user',.0),
//       createData('Ajay', 'Ajay@gmail.com', 'user',.0),
//       createData('Sharma', 'Sharma@gmail.com', 'user',.2),
//       createData('Abhishekh', 'Abhishekh@gmail.com', 'user', 2),
//       createData('Shivni', 'Shivni@gmail.com', 'user',.0),
//       createData('Amit', 'Amit@gmail.com', 'user',.0),
//     ].sort((a, b) => (a.calories < b.calories ? -1 : 1)),
//     page: 0,
//     rowsPerPage: 5,
//   };

//   handleChangePage = (event, page) => {
//     this.setState({ page });
//   };

//   handleChangeRowsPerPage = event => {
//     this.setState({ rowsPerPage: event.target.value });
//   };

//   render() {
//     const { classes } = this.props;
//     const { rows, rowsPerPage, page } = this.state;
//     const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

//     return (
//       <Wrapper>
//       <Paper className={classes.root}>
//         <div className={classes.tableWrapper}>
//           <Table className={classes.table}>
//             <TableBody>
//               {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
//                 return (
//                   <TableRow key={row.id}>
//                     <TableCell component="th" scope="row">
//                       {row.name}
//                     </TableCell>
//                     <TableCell numeric>{row.calories}</TableCell>
//                     <TableCell numeric>{row.fat}</TableCell>
//                   </TableRow>
//                 );
//               })}
//               {emptyRows > 0 && (
//                 <TableRow style={{ height: 48 * emptyRows }}>
//                   <TableCell colSpan={6} />
//                 </TableRow>
//               )}
//             </TableBody>
//             <TableFooter>
//               <TableRow>
//                 <TablePagination
//                   colSpan={3}
//                   count={rows.length}
//                   rowsPerPage={rowsPerPage}
//                   page={page}
//                   onChangePage={this.handleChangePage}
//                   onChangeRowsPerPage={this.handleChangeRowsPerPage}
//                   ActionsComponent={TablePaginationActionsWrapped}
//                 />
//               </TableRow>
//             </TableFooter>
//           </Table>
//         </div>
//       </Paper>
//       </Wrapper>
//     );
//   }
// }

// AddProduct.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

// export default withStyles(styles)(AddProduct);








import React from 'react';
import { Wrapper } from '../../components/'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { TextField, Select, FormControl, Card, Grid, Button, Typography } from '@material-ui/core';
import styles from './AddProduct.styles';
import {addProduct} from '../../redux/actions/product';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { useSnackbar,withSnackbar } from "notistack";


class AddProduct extends React.Component {
  state = {
    product: {
      name: '',
      category: '',
      tags: '',
      images: [],
      images_url: []
    }
  };

  handleCategoryChange = (e) => {
    e.persist()
    const { product } = this.state;
    product.category = e.target.value

    this.setState({ product })
  }

  handleNameChange = (e) => {
    e.persist()
    const { product } = this.state;
    product.name = e.target.value

    this.setState({ product })
  }

  handleTagsChange = (e) => {
    e.persist()
    const { product } = this.state;
    product.tags = e.target.value
    this.setState({ product })
  }

  handleImageChooseFile = (e) => {
    e.persist()
    const { product } = this.state;
    product.images.push(e.target.files[0])
    const file = e.target.files[0]
    const reader = new FileReader();

    reader.onloadend = () => {
      product.images_url.push(reader.result)
      this.setState({ product })
    };

    reader.readAsDataURL(file);
  }

  handleImageRemove = (e, index) => {
    e.persist()
    const { product } = this.state;
    product.images.splice(index, 1);
    product.images_url.splice(index, 1);
    this.setState({ product })
  }

  handleSubmit = (e) => {
    const { product } = this.state
    let newFormData = new FormData()
    newFormData.append('product_name', product.name);
    newFormData.append('product_type',product.category);
    newFormData.append('product_price', 100);
    newFormData.append('products_date_available',product.category);
    newFormData.append('manufacturers_id', 1);
    newFormData.append('low_limit', 20);
    newFormData.append('is_feature',1);
    newFormData.append('category_image', product.images[0])
    product.tags = product.tags.replace(/ /g, '').split(',');
    this.props.addProduct(newFormData).then(response => {
      this.props.enqueueSnackbar('Successfully Added the data');
      product.name = '';
      product.category = '';
      product.images = []
    }).catch(error => {
      this.props.enqueueSnackbar('Failed to add')
    })
  }

  handleOnReset = (e) => {
    this.setState({
      product: {
        name: '',
        category: '',
        tags: '',
        images: [],
        images_url: []
      }
    })
  }


  render() {
    const { classes } = this.props;
    const { } = this.state;

    return (
      <Wrapper className={classes.wrapper}>
        <Paper className={classes.root}>
          <Grid md={12} item>
            <h3 className={classes.formHeading}>Product Add Form</h3>
          </Grid>
          <Grid md={12} item>
            <Grid md={8} item className={classes.formGrid}>

              <FormControl className={classes.formControl}>
                <TextField
                  autoFocus
                  margin="dense"
                  name="product_name"
                  id="product_name"
                  label="product_name"
                  type="text"
                  fullWidth
                  value={this.state.product.name}
                  onChange={this.handleNameChange}
                />
              </FormControl>
              <FormControl className={classes.formControl}>
                <Select
                  defaultValue={this.state.product.category}
                  onChange={this.handleCategoryChange}
                  label="Category" fullWidth native defaultValue="" id="product-category">
                  <option value=""> Select Category </option>
                  <option value={'category 1'}>category 1</option>
                  <option value={'category 2'}>category 2</option>
                  <option value={'category 3'}>category 3</option>
                  <option value={'category 4'}>category 4</option>
                </Select>
              </FormControl>

              <FormControl className={classes.formControl}>
                <TextField
                  onChange={this.handleTagsChange}
                  margin="dense"
                  name="tags"
                  id="tags"
                  type="text"
                  label="Tagss (comma saperated):"
                  fullWidth
                />
              </FormControl>
              <FormControl>
                <label className={`${classes.formControlBtn}`}>
                    <TextField
                      onChange={this.handleImageChooseFile}
                      margin="dense"
                      id="product-images"
                      type="file"
                      className={classes.imageFormControl}
                    />
                    Choose File
                </label>
              </FormControl>
              {
                this.state.product.images_url && this.state.product.images_url.length ? (
                  <Grid md={12} item className={`${classes.productImagesContainer} ${classes.my10}`} item>
                    {this.state.product.images_url.map((img, idx) => {
                      return (
                        <Grid md={3} key={idx} className={classes.productImages}>
                          <Card>
                            <span onClick={(e) => { this.handleImageRemove(e, idx) }} className={classes.crossIcon}>X</span>
                            <img className={classes.productImage} src={img}></img>
                          </Card>
                        </Grid>
                      )
                    })}
                  </Grid>
              ) : null }
            </Grid>
          </Grid>
          <Grid md={12} item>
            <Typography md={12} align="center">

              <Button className={classes.mxy10} onClick={(e) => { this.handleSubmit(e) }} variant="contained" color="primary">
                Submit
            </Button>
              <Button className={classes.mxy10} onClick={(e) => { this.handleOnReset(e) }} variant="contained">Reset</Button>
            </Typography>
          </Grid>
        </Paper>
      </Wrapper>
    );
  }
}

AddProduct.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(null, {addProduct}), withStyles(styles), withSnackbar)(AddProduct);


