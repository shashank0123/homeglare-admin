import React, { Component } from 'react';
import { Card, CardBody, Badge,Col,InputGroup,InputGroupAddon,Input,UncontrolledDropdown,DropdownToggle,DropdownMenu,DropdownItem,Button, Row} from 'reactstrap';
import DataTable from 'react-data-table-component';
import { ChevronDown, Check,Search } from 'react-feather';
import Checkbox from '../../../components/@vuexy/checkbox/CheckboxesVuexy';
import "../../../assets/scss/plugins/extensions/react-paginate.scss"
import "../../../assets/scss/pages/data-list.scss"
import { connect } from 'react-redux';
import { categories } from '../../../redux/actions/categories';
const CustomHeader = props => {
  return (
    <div className="data-list-header d-flex justify-content-between flex-wrap">
      <div className="actions-left d-flex flex-wrap">
       <h3>All Categories</h3>
        {/* <Button
          className="add-new-btn"
          color="primary"
          onClick={() => props.handleSidebar(true, true)}
          outline>
          <Plus size={15} />
          <span className="align-middle">Add New</span>
        </Button> */}
      </div>
    </div>
  )
}

class ViewCategory extends Component {
  state = {
    data: [],
    totalPages: 1,
    currentPage: 0,
    columns: [
      {
        name: 'Category Name',
        selector: 'category_name',
        sortable: false,
        minWidth: '200px',
        cell: (row) => (
          <div className="d-flex  flex-column align-items-start py-xl-0 py-1">
            <div className="user-img ml-xl-0 ml-2">
              <img
                className="img-fluid rounded-circle"
                height="36"
                width="36"
                src={row.image}
                alt={row.name}
              />
            </div>
            <br />
            <div className="user-info text-truncate ml-0">
              <p
                title={row.name}
                className="d-block text-bold-600 text-truncate mb-0"
              >
                {row.name}
              </p>
            </div>
          </div>
        ),
      },
      {
        name: 'Medicine',
        selector: 'product',
        sortable: false,
        cell: (row) => (
          <p className="text-bold-600 text-truncate mb-0">{row.product}</p>
        ),
      },
      {
        name: 'Invoice',
        selector: 'invoice',
        sortable: false,
        cell: (row) => (
          <p className="text-bold-600 text-truncate mb-0">{row.invoice}</p>
        ),
      },
      {
        name: 'Price',
        selector: 'amount',
        sortable: false,
        cell: (row) => <p className="text-bold-600 mb-0">{row.amount}</p>,
      },
      {
        name: 'Status',
        selector: 'order_status',
        sortable: false,
        cell: (row) => (
          <Badge
            outline
            className="border-primary"
            style={{backgroundColor:"#f1f1f3",fontWeight:"600",fontSize:"12px"}}
            color={row.status === 'inactive' ? 'light-danger' : 'light-primary'}
          >
            {row.order_status}
          </Badge>
        ),
      },
     
    ],
    allData: [],
    value: '',
    rowsPerPage: 4,
    sidebar: false,
    currentData: null,
    selected: [],
    totalRecords: 0,
    sortIndex: [],
    addNew: '',
  };
  componentDidMount(){
    this.props.categories()
  }
  render() {
    let { columns, data } = this.state;
    console.log(this.props.categories)
    return (
      <React.Fragment>
        <Row>
        <Col sm="10" className="mb-2">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
              <Button color="primary">
                  {/* <Search size={20} /> */}
                </Button>
              </InputGroupAddon>
              <Input />
            </InputGroup>
          </Col>
          <Col sm="2">
          <Button color="primary" style={{
            fontWeight:"600"
          }}>
                  Search
                </Button>
          </Col>
        </Row>
        
          <Card>
        <CardBody>
        <div
        className="data-list">
          <DataTable
            columns={columns}
            data={this.props.categories}
            pagination
            paginationServer
            //   paginationComponent={() => (
            //     <ReactPaginate
            //       previousLabel={<ChevronLeft size={15} />}
            //       nextLabel={<ChevronRight size={15} />}
            //       breakLabel="..."
            //       breakClassName="break-me"
            //       pageCount={totalPages}
            //       containerClassName="vx-pagination separated-pagination pagination-end pagination-sm mb-0 mt-2"
            //       activeClassName="active"
            //       forcePage={
            //         this.props.parsedFilter.page
            //           ? parseInt(this.props.parsedFilter.page - 1)
            //           : 0
            //       }
            //       onPageChange={page => this.handlePagination(page)}
            //     />
            //   )}
            pagination={false}
            noHeader
            subHeader
            //selectableRows
            responsive
            pointerOnHover
            selectableRowsHighlight
            //   onSelectedRowsChange={data =>
            //     this.setState({ selected: data.selectedRows })
            //   }
            //customStyles={selectedStyle}
            subHeaderComponent={
              <CustomHeader
                // handleSidebar={this.handleSidebar}
                // handleFilter={this.handleFilter}
                // handleRowsPerPage={this.handleRowsPerPage}
                // rowsPerPage={rowsPerPage}
                // total={totalRecords}
                // index={sortIndex}
              />
            }
            sortIcon={<ChevronDown />}
            //selectableRowsComponent={Checkbox}
            // selectableRowsComponentProps={{
            //   color: 'primary',
            //   icon: <Check className="vx-icon" size={12} />,
            //   label: '',
            //   size: 'sm',
            // }}
          />
          </div>
        </CardBody>
      </Card>
    
      </React.Fragment>
   );
  }
}
const mapStateToProps = (state) => {
  console.log(state.category.categories)
  return {
    loading: state.auth,
    categories:state.category.categories
  };
};

export default connect(mapStateToProps, {
  categories
})(ViewCategory);

