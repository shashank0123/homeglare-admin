// import React, { Component } from 'react';
// import {
//   Card,
//   CardBody,
//   CardHeader,
//   Media,
//   CardTitle,
//   CustomInput,
//   Row,
//   Col,
//   FormGroup,
//   Label,
//   Input,
// } from 'reactstrap';
// import { connect } from 'react-redux';
// import { createCategory } from '../../redux/actions/categories';
// import { Link } from 'react-router-dom';

// class AddCategory extends Component {
//   state = {
//     category_slug: '',
//     category_name: '',
//     category_status: '1',
//     gstin: '',
//     branch_name: '',
//     account_number: '',
//     pancard: '',
//     getValue: '',
//   };
//   static getDerivedStateFromProps(props, state) {
    

//     return null;
//   }
//   handleSubmit() {
//     const value = {
//       category_slug: this.state.category_slug,
//       category_name: this.state.category_name,
//       category_status: this.state.category_status,
     
//     };
//     this.props.createCategory(value);
//   }
//   componentDidMount() {
//     // this.props.getBankDetails();
//   }
//   render() {
//     const {
//       category_slug,
//       category_name,
//       category_status,      
//     } = this.state;
//     return (
//       <Card>
//         <CardHeader>
//           <div className="title">
//             <CardTitle>Add Category</CardTitle>
//           </div>
//         </CardHeader>
//         <CardBody>
//           <Row>
//             <Col lg="6" md="6" sm="12">
//               <FormGroup>
//                 <Label for="basicInput">Category Name</Label>
//                 <Input
//                   type="text"
//                   id="basicInput"
//                   value={category_name}
//                   onChange={(e) => {
//                     this.setState({
//                       category_name: e.target.value,
//                     });
//                   }}
//                 />
//               </FormGroup>
//             </Col>
//             <Col lg="6" md="6" sm="12">
//               <FormGroup>
//                 <Label for="basicInput">Category slug</Label>
//                 <Input
//                   type="text"
//                   id="basicInput"
//                   value={category_slug}
//                   onChange={(e) => {
//                     this.setState({
//                       category_slug: e.target.value,
//                     });
//                   }}
//                 />
//               </FormGroup>
//             </Col>
//             <Col lg="6" md="6" sm="12">
//               <FormGroup>
//                 <Label for="basicInput">Status</Label>
//                 <Input
//                   type="text"
//                   id="basicInput"
//                   value={category_status}
//                   onChange={(e) => {
//                     this.setState({
//                       category_status: e.target.value,
//                     });
//                   }}
//                 />
//               </FormGroup>
//             </Col>
            
//             <Col lg="12" md="12" sm="12" className="account-footer">
//               <FormGroup>
//                 <div className="button-footer">
//                   <button
//                     className="btn reset-all"
//                     onClick={() => {
//                       console.log('here');
//                     }}
//                     style={{ fontWeight: '600' }}
//                   >
//                     Reset All
//                   </button>
//                   <button
//                     className="btn btn-primary float-right "
//                     onClick={() => this.handleSubmit()}
//                     style={{ fontWeight: '600' }}
//                   >
//                     Save Changes
//                   </button>
//                 </div>
//               </FormGroup>
//             </Col>
//           </Row>
//         </CardBody>
//       </Card>
//     );
//   }
// }
// // const mapStateToProps = (state) => {
// //   return {
// //   };
// // };

// export default connect(null, {
//   createCategory,
// })(AddCategory);




import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Link } from "react-router-dom";
import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from 'react-redux';
import { createCategory } from '../../redux/actions/categories';
import { useSnackbar,withSnackbar } from "notistack";

const useStyles = makeStyles(theme => ({
  card: {
    overflow: "visible"
  },
  session: {
    position: "relative",
    zIndex: 4000,
    // minHeight: "100vh",
    display: "flex",
    flexDirection: "column"
  },
//   background: {
//     backgroundColor: theme.palette.primary.main
//   },
  content: {
    padding: `40px ${theme.spacing(1)}px`,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: "1 0 auto",
    flexDirection: "column",
    minHeight: "100%",
    textAlign: "center"
  },
  wrapper: {
    flex: "none",
    maxWidth: "400px",
    width: "100%",
    margin: "0 auto"
  },
  fullWidth: {
    width: "100%"
  },
  logo: {
    display: "flex",
    flexDirection: "column"
  }
}));

function CategoryModal(props)  {

  const[category_slug, setSlug] =  useState('');
  const[category_name, setName] =  useState('');
  const[category_status, setStatus] = useState('');
  const dispatch = useDispatch();
//const { enqueueSnackbar } = useSnackbar();



  const handleSubmit = () => {
    const value = {
      category_slug,
      category_name,
      category_status
    };
    console.log('valueeeeeeeee 214', value);
    //this.props.createCategory(value);
    dispatch(createCategory(value)).then(response => {
      console.log('21888', response);
      setSlug('');
      setName('');
      setStatus('');
     props.enqueueSnackbar('Category Addeed sucessfully')
    }).catch(error => {
      console.log('erriorrrrr', error)
      props.enqueueSnackbar('Something went wrong')
    })
  }



  const classes = useStyles();

  return (
    <div className={classNames(classes.session, classes.background)}>
      <div className={classes.content}>
        <div className={classes.wrapper}>
          <Card>
            <CardContent>
                <TextField
                  id="categoryName"
                  label="Category Name"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  onChange = {e => setName(e.target.value)}
                />
                <TextField
                  id="category_slug"
                  label="Category slug"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  onChange = {e => setSlug(e.target.value)}
                />
                <TextField
                  id="status"
                  label="Status"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  onChange = {e => setStatus(e.target.value)}
                />
                  <Button
                  variant="contained"
                  color="primary"
                  fullWidth
                  type="submit"
                 onClick={() => handleSubmit()}
                >
                  Add Category
                </Button>       
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
  }
export default withSnackbar(CategoryModal);
