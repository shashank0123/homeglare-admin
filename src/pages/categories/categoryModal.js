import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Link } from "react-router-dom";
import React from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  card: {
    overflow: "visible"
  },
  session: {
    position: "relative",
    zIndex: 4000,
    // minHeight: "100vh",
    display: "flex",
    flexDirection: "column"
  },
  content: {
    padding: `40px ${theme.spacing(1)}px`,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: "1 0 auto",
    flexDirection: "column",
    minHeight: "100%",
    textAlign: "center"
  },
  wrapper: {
    flex: "none",
    maxWidth: "400px",
    width: "100%",
    margin: "0 auto"
  },
  fullWidth: {
    width: "100%"
  },
  logo: {
    display: "flex",
    flexDirection: "column"
  }
}));

const CategoryModal = (props) => {
  const classes = useStyles();
  const {selectedRow} = props
  return (
    <div className={classNames(classes.session, classes.background)}>
      <div className={classes.content}>
        <div className={classes.wrapper}>
          <Card>
            <CardContent>
                <TextField
                  id="id"
                  label="Id"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  value={selectedRow.id}
                />
                <TextField
                  id="category_name"
                  label="Category Name"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  value={selectedRow.category_name}
                />
                <TextField
                  id="createdDate"
                  label="Created Date"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  value={selectedRow.created_at}
                />
                  <TextField
                  id="updatedDate"
                  label="Updated Date"
                  className={classes.textField}
                  fullWidth
                  margin="normal"
                  value={selectedRow.updated_at}
                />
              
               
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default CategoryModal;
