const urlObj = {
    login_url: 'http://159.65.153.124/api/admin/login',
    validate_token:"http://159.65.153.124/api/admin/validate-token",
    get_profile:'http://159.65.153.124/api/admin/profile',
    banks:'http://159.65.153.124/api/admin/banks',//GET POST DELETE PUT->update
    completed_order:'http://159.65.153.124/api/admin/order/completed',
    cancelled_order:'http://159.65.153.124/api/admin/order/canceled',
    recent_order:'http://159.65.153.124/api/admin/order/recently',
    appointment:'http://159.65.153.124/api/admin/appointments',
    bank_detail:'http://159.65.153.124/api/admin/banks',
    dashboard:'http://159.65.153.124/api/admin/dashboard',
    create_category:'http://159.65.153.124/api/admin/categories',
    get_product: 'http://159.65.153.124/api/admin/products'

    

  };
  
  module.exports = urlObj;