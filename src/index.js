
import { Provider } from "react-redux"
import { store } from "./redux/storeConfig/store"
import AppProvider from "./components/AppProvider/AppProvider";

import React from "react";
import registerServiceWorker from "./registerServiceWorker";
import { render } from "react-dom";
import setAuthToken from "./utility/settoken"
import {validate,loadingfalse} from "./redux/actions/auth/loginActions"
import Router from './Router'
import { SnackbarProvider } from 'notistack';

// Check for token to keep user logged in
if (localStorage.token) {
  const token = localStorage.token;
  setAuthToken(token);
  store.dispatch(validate(token));
}
else{
  store.dispatch(loadingfalse())
}
render(
  <Provider store={store}>
    <SnackbarProvider>
  <AppProvider>
    <Router />
  </AppProvider>
  </SnackbarProvider>
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
