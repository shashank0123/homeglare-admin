const initialState = {
    productData : [],
    addedProduct: {}
}

export const productReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'FETCH_ALL_PRODUCT': {
            return {
                ...state,
                productData:action.payload
            }
        }
        case 'ADD_PRODUCT': {
            return {
                ...state,
                addedProduct:action.payload
            }
        }
        default: {
            return state
          }
    }
}