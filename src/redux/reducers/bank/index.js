const initialState = {
  bank_details: {}
};

export const bank = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_BANK_DETAILS': {
      return { ...state, bank_details: action.payload,getValue:'yes' };
    }
    default: {
      return state;
    }
  }
};
export default bank;
