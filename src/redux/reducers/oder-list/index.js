const initialState = {
  completed_order: [],
  cancelled_order: [],
  recent_order: [],
  appointments: [],
};

const calculate = (value) => {
  const result=[]
  console.log("uduid",value)
  value.map((index) => {
    const date = index.appointment_date.split(' ');
    result.push({name:index.name,appointment_status:index.appointment_status,disease:index.disease,id:index.id,test:index.test,date:date[0],time:date[1]})
    
    console.log(date[0],date[1])
    value.push({date:date[0],time:date[1]})
  });
  return result;
};

export const orderList = (state = initialState, action) => {
  switch (action.type) {
    case 'COMPLETED_ORDER': {
      return { ...state, completed_order: action.payload };
    }
    case 'CANCELLED_ORDER': {
      return { ...state, cancelled_order: action.payload };
    }
    case 'RECENT_ORDER': {
      return { ...state, recent_order: action.payload };
    }
    case 'APPOINTMENT': {
      return { ...state, appointments: calculate(action.payload) };
    }
    default: {
      return state;
    }
  }
};
export default orderList;
