const initialState = {
  revenue: '',
  summary:'',
  total_order:'',
  total_order_sales:[],
  revenue_expenses:[],
  revenue_income:[],
  revenue_months:[]
};

export const bank = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DASHBOARD_DETAILS': {
      return { ...state, data:action.payload };
    }
    default: {
      return state;
    }
  }
};
export default bank;
