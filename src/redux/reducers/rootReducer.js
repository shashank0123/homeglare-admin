import { combineReducers } from "redux"
// import customizer from "./customizer/"
import auth from "./auth/"
import navbar from "./navbar/Index"
import orderList from "./oder-list/index"
import bank from "./bank"
import dashboard from "./dashboard"
import category from "./categories"
import {productReducer} from './product'

const rootReducer = combineReducers({
  // customizer: customizer,
  auth: auth,
  navbar: navbar,
  orderList:orderList,
  bank:bank,
  category:category,
  dashboard:dashboard,
  product: productReducer
})

export default rootReducer
