const initialState = {
  category: {}, 
  categories:[]
};

export const category = (state = initialState, action) => {
  switch (action.type) {
    case 'CREATE_CATEGORY': {
      return { ...state, category: action.payload,getValue:'yes' };
    }
    case 'CATEGORY': {
      return { ...state, categories: action.payload,getValue:'yes' };
    }
    default: {
      return state;
    }
  }
};
export default category;
