import axios from 'axios';
import url from '../../../utility/url';
import { reject } from 'lodash';

export const getProduct = () => {
    return async (dispatch) => {
        await axios.get(url.get_product).then(response => {
            console.log('responseeee', response);
            dispatch({type: 'FETCH_ALL_PRODUCT', payload: response.data.data})
        })
    }
}

export const addProduct = (value) => dispatch => {
    console.log('valueeee', value);
    return new Promise((resolve, reject) => {
        axios.post(url.get_product, value).then(response => {
            dispatch({type: ' ADD_PRODUCT', payload: response.data.data})
            resolve(response)
        }).catch(error => {
            reject(error)
        })
    })
    // return async (dispatch) => {
    //     await axios.post(url.get_product, value).then(response => {
    //         console.log('post responseeee', response);
    //     })
    // }
}