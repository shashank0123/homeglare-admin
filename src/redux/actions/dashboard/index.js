import axios from 'axios';
import url from '../../../utility/url';
export const getDashboard = () =>{
  return async (dispatch, getState) => {
    await axios
      .get(url.dashboard)
      .then((response) => {
        console.log(response)
        dispatch({
          type:"GET_DASHBOARD_DETAILS",
          payload:response.data.data,
          
        })
      })
      .catch((error) => {
        console.log(error);
      });
  };
}
