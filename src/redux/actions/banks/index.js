import axios from 'axios';
import url from '../../../utility/url';
export const getBankDetails = () => {
  return async (dispatch, getState) => {
    await axios
      .get(url.bank_detail)
      .then((response) => {
        dispatch({
          type: 'GET_BANK_DETAILS',
          payload: response.data.data,
          
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const updateBankDetails = (value) => {
  return async (dispatch, getState) => {
    await axios
      .post(url.bank_detail,value)
      .then((response) => {
        dispatch({
          //type: 'GET_BANK_DETAILS',
          //payload: response.data.data[0],
          
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
