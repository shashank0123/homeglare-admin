import axios from 'axios';
import url from '../../../utility/url';
import { useSnackbar } from "notistack";
import { reject } from 'lodash';
export const createCategory = (value) =>dispatch => {
  return new Promise((resolve, reject) => {
        axios
      .post(url.create_category, value)
      .then((response) => {
        console.log('addproductresponseeeeee', response);
        dispatch({
          type: 'CREATE_CATEGORY',
          payload: response.data,
          
        });
        resolve(response)
      })
      .catch((error) => {
        console.log(error);
        reject(error)
      });
  })
  // return async (dispatch, getState) => {

  //   await axios
  //     .post(url.create_category, value)
  //     .then((response) => {
  //       dispatch({
  //         type: 'CREATE_CATEGORY',
  //         payload: response.data,
          
  //       });
  //       console.log(response);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };
};


export const categories = () => {
  return async (dispatch, getState) => {
    await axios
      .get(url.create_category)
      .then((response) => {
        dispatch({
          type: 'CATEGORY',
          payload: response.data.data,
          
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

