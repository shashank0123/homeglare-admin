import axios from "axios"
import url from "../../../utility/url"
import {history} from "../../../history"
import setToken from "../../../utility/settoken"

export const login = value =>{
  return async (dispatch, getState) => {
    await axios
      .post(url.login_url,value)
      .then(async (response)  => {
        console.log(response.status===200);
        if(response.status===200){
         await setToken(response.data.token)
        dispatch({
          type: 'LOGIN_WITH_EMAIL',
          payload: response.data.token,
        });
        const  token  = response.data.token;
        localStorage.setItem("token", token);
        dispatch(getProfile())
     history.push("/")
      }
      else{
        dispatch({
          type: 'LOGIN_WITH_ERROR',
          payload: response,
        });
      }
      })
      .catch((error) => {
        dispatch({
          type: 'LOGIN_WITH_ERROR',
         // payload: error.response.data,
        });
      });
  };
}
export const getProfile = () =>{
  return async (dispatch, getState) => {
    await axios
      .get(url.get_profile)
      .then((response) => {
        console.log(response.data.data)
        dispatch({
          type:"GET_PROFILE",
          payload:response.data.data
        })
      })
      .catch((error) => {
        console.log(error);
      });
  };
}
export const updateProfile = () =>{
  return async (dispatch, getState) => {
    await axios
      .post(url.get_profile)
      .then((response) => {
        console.log(response)
        dispatch({
          //type:"GET_PROFILE",
          //payload:response.data.data
        })
      })
      .catch((error) => {
        //console.log(error);
      });
  };
}

export const loadingfalse = value =>{
  return async (dispatch, getState) => {
    dispatch({
      type:"LOADING_FALSE"
    })
  };
}


export const validate = value =>{
  return async (dispatch, getState) => {
    await axios
      .get(url.validate_token,value,{headers:{
        Authorization:localStorage.getItem('token')
      }})
      .then((response) => {
        console.log(response.status===200);
        if(response.status===200){
          console.log(localStorage.getItem('token'))
          dispatch(getProfile())
        dispatch({
          type: 'LOGIN_VALIDATE',
          //payload: localStorage.getItem('token'),
        });
        dispatch(loadingfalse())
        setToken(localStorage.getItem('token'))
        const  token  = localStorage.getItem('token');
        localStorage.setItem("token", token);
        history.push("/")
    
      }
      else{
        dispatch({
          type: 'LOGIN_WITH_ERROR',
          payload: response,
        });
      }
      })
      .catch((error) => {
        console.log()
       if(!(error.message==="Network Error")) {
        if(error.response.data.message==="Unauthenticated."){
          localStorage.removeItem("token");
          //console.log("here")
        }
        }
        
        dispatch({
          type: 'LOGIN_WITH_ERROR',
           payload: error,
        });
      });
  };
}
// Log user out
export const logoutUser =  () => async dispatch => {
  // Remove token from local storage
  localStorage.removeItem('token');
  // Remove auth header for future requests
  setToken(false);
  dispatch({
    type:"LOGOUT"
  })
  // Set current user to empty object {} which will set isAuthenticated to false
 // await dispatch(setCurrentUser({}));

 history.push('./login');
};


