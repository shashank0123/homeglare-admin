import React from 'react';
import { BrowserRouter,HashRouter, Switch, Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { connect } from 'react-redux';
import Dashboard from "./containers/Dashboard";
import {createBrowserHistory} from 'history'
import {
  BackendError,
  Lockscreen,
  NotFound,
  PasswordReset,
  Signin,
  Signup
} from "./pages";

// Set Layout and Component Using App Route
const RouteConfig = ({
  component: Component,
  fullLayout,
  permission,
  user,
  ...rest
}) => {
   return (
  <Route
    {...rest}
    render={(props) => {
      console.log('userrrrr', user);
      return (
        <React.Fragment>
          {user.loading?'':
          user.isAuthenticated  ? 
            
                 (
                  <React.Fragment>
                        <Component {...props} />
                  </React.Fragment>
                )
              
          : (
            <React.Fragment>
                <Redirect to="/signin" />
                <Component {...props} />
            </React.Fragment>
          )}
        </React.Fragment>
      );
    }}
  />
)};
const mapStateToProps = (state) => {
  return {
    user: state.auth.login,
  };
};

const AppRoute = connect(mapStateToProps, null)(RouteConfig);

class AppRouter extends React.Component {
  render() {
    console.log('approuteee');
    return (
      <BrowserRouter basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route exact path="/404" component={NotFound} />
        <Route exact path="/500" component={BackendError} />
        <Route exact path="/Lockscreen" component={Lockscreen} />
        <Route exact path="/forgot" component={PasswordReset} />
        <Route exact path="/signin" component={Signin} />
        <Route exact path="/signup" component={Signup} />
        {/* <Route exact path="/" component={Dashboard}/> */}
      <AppRoute  path="/" component={Dashboard} />

      </Switch>
     </BrowserRouter>
    );
  }
}

export default AppRouter;
